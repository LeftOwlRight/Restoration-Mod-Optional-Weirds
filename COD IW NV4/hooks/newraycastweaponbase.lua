Hooks:PostHook(NewRaycastWeaponBase, "set_ammo_remaining_in_clip", "nova4_iw_elo_ammo_count", function(self)

	if self._scopes and self._parts then
		local part = nil
		for i, part_id in ipairs(self._scopes) do
			local mag_count = self:get_ammo_remaining_in_clip()
			local mag_size = self:calculate_ammo_max_per_clip()
			local ammo_percent = mag_count / mag_size * 100
			--log("current mag percent: " .. ammo_percent .. "%")
			part = self._parts[part_id]
			if part and part.unit:nova4_iw_elo_ammocount() then
				part.unit:nova4_iw_elo_ammocount():number_set(mag_count)
		--		if ammo_percent > 20 then
		--			part.unit:nova4_iw_elo_ammocount():set_color_type("light_blue")
		--			part.unit:get_object(Idstring("g_lens_op")):set_visibility(true)
		--			part.unit:get_object(Idstring("g_lens_op_low_ammo")):set_visibility(false)
		--		else
		--			part.unit:nova4_iw_elo_ammocount():set_color_type("red")
		--			part.unit:get_object(Idstring("g_lens_op")):set_visibility(false)
		--			part.unit:get_object(Idstring("g_lens_op_low_ammo")):set_visibility(true)
		--		end
			end
		end
	end
end)