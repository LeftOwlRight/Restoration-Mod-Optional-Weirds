Hooks:PostHook(WeaponFactoryTweakData, "init", "nova4_mod_init", function(self)

--  Reflex
	local custom_sight_id = "wpn_fps_ass_nova4_iw_reflex"
	local based_on_id = "wpn_fps_upg_o_specter"
	local offset = Vector3(0, -1, 1.5)
	if self.parts[custom_sight_id].stance_mod then
		for _, wpn_id in pairs(self.parts[custom_sight_id].stance_mod) do
			wpn_id.translation = (wpn_id.translation + offset)
	end
	end
	for _, part_id in pairs(self.parts) do
		if part_id.override and part_id.override[based_on_id] then
			part_id.override[custom_sight_id] = deep_clone(part_id.override[based_on_id])
			if part_id.override[custom_sight_id].stance_mod then
			for _, wpn_id in pairs(part_id.override[custom_sight_id].stance_mod) do
					wpn_id.translation = (wpn_id.translation + offset)
				end
			end
		end
	end 

--  ELO
	local custom_sight_id = "wpn_fps_ass_nova4_iw_elo"
	local based_on_id = "wpn_fps_upg_o_specter"
	local offset = Vector3(0, 0, 0)
	if self.parts[custom_sight_id].stance_mod then
		for _, wpn_id in pairs(self.parts[custom_sight_id].stance_mod) do
			wpn_id.translation = (wpn_id.translation + offset)
	end
	end
	for _, part_id in pairs(self.parts) do
		if part_id.override and part_id.override[based_on_id] then
			part_id.override[custom_sight_id] = deep_clone(part_id.override[based_on_id])
			if part_id.override[custom_sight_id].stance_mod then
			for _, wpn_id in pairs(part_id.override[custom_sight_id].stance_mod) do
					wpn_id.translation = (wpn_id.translation + offset)
				end
			end
		end
	end 	
	
end)