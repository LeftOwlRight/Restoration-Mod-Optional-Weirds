{
	"bm_melee_therkatana" : "Arasaka Thermal Katana",
    "bm_melee_therkatana_info" : "It would appear that Arasaka have shown interest in a more advanced variant, resulting in a military variant of the katana that allowed the blade to be equipped with exotic modifications, such as Thermal damage.",

	"menu_l_global_value_cyberpunkmod" : "This is a mod by Hybrid!"
}