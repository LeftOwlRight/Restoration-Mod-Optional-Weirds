local THIS_VERSION = 2

if MisriahAmmoGui and MisriahAmmoGui.VERSION and (MisriahAmmoGui.VERSION >= THIS_VERSION) then 
	log("(Halo 2 Battle Rifle) MisriahAmmoGui version " .. tostring(MisriahAmmoGui.VERSION) .. " already exists")
	return
end

MisriahAmmoGui = MisriahAmmoGui or class()

MisriahAmmoGui.VERSION = THIS_VERSION

function MisriahAmmoGui:init(unit)
	
	self._unit = self._unit or unit
	self._gui_object = self._gui_object or "a_counter"
	self.font_name = self.font_name or "fonts/font_medium_df"
	self.font_size = self.font_size or 64
	self.font_color = self.font_color or "ffffff"
	self._font_color = Color(self.font_color)
	
	self.font_offset_x = self.font_offset_x or 16
	self.font_offset_x_2 = self.font_offset_x_2 or 4
	self.font_offset_y = self.font_offset_y or 0 -- -15
	
	self.halign = self.halign or "left"
	self.valign = self.valign or "center"
	
	self.num_characters = self.num_characters or 2
	self.max_counter_value = self.max_counter_value or (math.pow(10,self.num_characters) - 1)
	
	self.offset_position_x = self.offset_position_x or 0
	self.offset_position_y = self.offset_position_y or 0
	self.offset_position_z = self.offset_position_z or 0
	
	self.offset_rotation_yaw = self.offset_rotation_yaw or 0
	self.offset_rotation_pitch = self.offset_rotation_pitch or 0
	self.offset_rotation_roll = self.offset_rotation_roll or 0
	
	self.PANEL_WIDTH = self.PANEL_WIDTH or 100
	self.PANEL_HEIGHT = self.PANEL_HEIGHT or 100
	self.WORLD_WIDTH = self.WORLD_WIDTH or 2
	self.WORLD_HEIGHT = self.WORLD_HEIGHT or 2
	
	self._new_gui = World:gui()
	
	
	local object_name = self._gui_object 
--	log("reachdmr: " .. tostring(object_name or "ERROR bad object name"))
	local object_ids = object_name and Idstring(object_name)
--	log("reachdmr: " .. tostring(object_ids or "ERROR bad object ids"))
	local object = object_ids and self._unit:get_object(object_ids)
--	log("reachdmr: " .. tostring(object or ("ERROR bad object " .. tostring(object_name))))
	if not object then 
		object = self._unit:orientation_object()
--		log("reachdmr: defaulting to orientation object")
	end
	
	if object then 
		self:add_workspace(object)
	end
end

function MisriahAmmoGui:add_workspace(gui_object)
	local onscreen_w = self.WORLD_WIDTH
	local onscreen_h = self.WORLD_HEIGHT
	local actual_w = self.PANEL_WIDTH
	local actual_h = self.PANEL_HEIGHT
	
	local ra = gui_object:rotation()
	
	local rb = Rotation(self.offset_rotation_yaw,self.offset_rotation_pitch,self.offset_rotation_roll)
	
	local rot = Rotation(ra:yaw() + rb:yaw(),ra:pitch() + rb:pitch(),ra:roll() + rb:roll())
	
	local x_axis = Vector3(onscreen_w,0,0)
	
	mvector3.rotate_with(x_axis,rot)
	
	local y_axis = Vector3(0,-onscreen_h,0)
	
	mvector3.rotate_with(y_axis,rot)
	
	local center = Vector3(onscreen_w / 2,onscreen_h / 2,0)
	
	mvector3.rotate_with(center,rot)
	
	local offset = Vector3(self.offset_position_x,self.offset_position_y,self.offset_position_z)
	
	mvector3.rotate_with(offset,rot)
	
	local position = gui_object:position()
	
	self._ws = self._new_gui:create_world_workspace(actual_w,actual_h,position,x_axis,y_axis)
	
	self._ws:set_linked(actual_w,actual_h,gui_object,position - center + offset,x_axis,y_axis)
	
	self._panel = self._ws:panel():panel({
		name = "ammo_counter_panel",
		layer = 1
	})
	
	--two separate text objects to handle the tens and the ones place of the ammo counter digits
	--this is to ensure spacing between characters, as it would appear on an analog display
	self._text_10 = self._panel:text({
		name = "text",
		font = self.font_name,
		font_size = self.font_size,
		text = "0",
		layer = 2,
		color = self._font_color,
		x = self.font_offset_x,
		y = self.font_offset_y,
		align = self.halign,
		vertical = self.valign or "center"
	})
	local _,_,t_w,_ = self._text_10:text_rect()
	self._text_1 = self._panel:text({
		name = "text",
		font = self.font_name,
		font_size = self.font_size,
		text = "0",
		layer = 2,
		color = self._font_color,
		x = self.font_offset_x + t_w + self.font_offset_x_2,
		y = self.font_offset_y,
		align = self.halign,
		vertical = self.valign or "center"
	})
	
	self._panel:rect({
		name = "debug rect",
		color = Color.blue,
		visible = false,
		alpha = 0.7,
		layer = 1
	})
	
end

function MisriahAmmoGui:set_visible(visible)
	if visible then 
		self._ws:show()
	else
		self._ws:hide()
	end
end

function MisriahAmmoGui:set_ammo_count(num)
	self:set_ammo_counter_text(string.format("%02i",math.clamp(num,0,self.max_counter_value)))
end

function MisriahAmmoGui:set_ammo_counter_text(text)
	self._text_10:set_text(string.sub(text,1,1))
	self._text_1:set_text(string.sub(text,-1,-1))
--	self._text:set_text(tostring(text)) --text:text(text).text (revolver ocelot)
--[[ --resize if ammo number is too big to fit (i ended up just capping the number of digits to two, aka 99 max display number)
	local x,y,w,h = self._text:text_rect()
	local wr = w / self._panel:w()
--	local hr = h / self._panel:h()
	if wr > 1 then 
		self._text:set_font_size(self._text:font_size() * (1 - (1/wr)))
	end
--]]
end

function MisriahAmmoGui:destroy()
	if alive(self._new_gui) and alive(self._ws) then
		self._new_gui:destroy_workspace(self._ws)

		self._ws = nil
		self._new_gui = nil
	end
end