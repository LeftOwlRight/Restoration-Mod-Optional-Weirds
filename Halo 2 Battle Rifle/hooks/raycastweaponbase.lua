Hooks:PostHook(RaycastWeaponBase,"set_ammo_remaining_in_clip","br55_set_ammo_counter",function(self,count)
	local unit = self._unit
	if unit:ammo_counter() then 
		unit:ammo_counter():set_ammo_count(count)
	end
end)