local THIS_VERSION = 2

if VakaraAmmoGui and VakaraAmmoGui.VERSION and VakaraAmmoGui.VERSION > THIS_VERSION then 
	return
end

--based on misriahammogui
--slightly too unique to use the actual misriahammogui class though
VakaraAmmoGui = VakaraAmmoGui or class()

VakaraAmmoGui.VERSION = THIS_VERSION

function VakaraAmmoGui:init(unit)
	self._unit = unit
	self.has_scope = nil
	
	self.scope_font_name = self.scope_font_name or "fonts/font_eurostile"
	
	self.max_num_digits = self.max_num_digits or 2
	self.max_counter_value = math.pow(10,self.max_num_digits) - 1
	
	self.scope_font_size = self.scope_font_size or 3
	self._scope_font_color = self._scope_font_color or "f0e982"
	self.scope_font_color = Color(self._scope_font_color)
	self.scope_kern = self.scope_kern or 4
	
	self.hip_font_size = self.hip_font_size or 24
	self._hip_font_color = self._hip_font_color or "fffd55" --alternately, "f6ec54"
	self.hip_font_color = Color(self._hip_font_color)
	self.hip_kern = self.hip_kern or -8
	self._scope_ws_params = {
		world_w = self.scope_world_w or 3,
		world_h = self.scope_world_h or 3,
		panel_w = self.scope_panel_w or 100,
		panel_h = self.scope_panel_h or 100,
		offset_x = self.scope_offset_x or 0.05,
		offset_y = self.scope_offset_y or 6,
		offset_z = self.scope_offset_z or 0,
		yaw = self.scope_yaw or 0,
		pitch = self.scope_pitch or 90,
		roll = self.scope_roll or 0
	}
	self._hip_ws_params = {
		world_w = self.hip_world_w or 1.4,
		world_h = self.hip_world_h or 3.5,
		panel_w = self.hip_panel_w or 50,
		panel_h = self.hip_panel_h or 3.5 * 100 / 3,
		offset_x = self.hip_offset_x or -4,
		offset_y = self.hip_offset_y or 0.75,
		offset_z = self.hip_offset_z or 10,
		yaw = self.hip_yaw or 0,
		pitch = self.hip_pitch or 90,
		roll = self.hip_roll or 0
	}
	
	self.scope_bg_alpha = self.scope_bg_alpha or 0.75
	self.scope_bg_texture = self.scope_bg_texture or "units/mods/weapons/wpn_fps_ass_vk78_commando_pts/vk78_scope_glass_df"
	
	if self.scope_counter_enabled == nil then
		--enabled by default
		self.scope_counter_enabled = true
	end
	if self.hip_counter_enabled == nil then
		--enabled by default
		self.hip_counter_enabled = true
	end
	self.hip_ammo_gradient_texture = self.hip_ammo_gradient_texture or "units/mods/weapons/wpn_fps_ass_vk78_commando_pts/vk78_hip_ammo_gradient"
	self.hip_ammo_gradient_texture_rect = self.hip_ammo_gradient_texture_rect or {0,0,64,64}
	self.hip_ammo_gradient_w = self.hip_ammo_gradient_w or 46
	self.hip_ammo_gradient_h = self.hip_ammo_gradient_h or 20
	self._hip_ammo_gradient_color = self._hip_ammo_gradient_color or "00baff"
	self.hip_ammo_gradient_color = Color(self._hip_ammo_gradient_color)
	self.hip_ammo_gradient_alpha = self.hip_ammo_gradient_alpha or 0.25
	
	--[[
	self.hip_text_gradient_texture = "units/mods/weapons/wpn_fps_ass_vk78_commando_pts/vk78_hip_ammo_gradient"
	self.hip_text_gradient_w = 40
	self.hip_text_gradient_h = 20
	self.hip_text_gradient_color = Color("f0e982")
	self.hip_text_gradient_alpha = 0.25
	--]]
	
	self._hip_text_rect_color = self._hip_text_rect_color or "fffd55"
	self.hip_text_rect_color = Color(self._hip_text_rect_color)
	self.hip_text_rect_w = self._hip_text_rect_w or 18
	self.hip_text_rect_h = self.hip_text_rect_h or 28
	self.hip_text_rect_alpha = self.hip_text_rect_alpha or 0.1
	self.hip_font_name = self.hip_font_name or "fonts/font_eurostile"
	
	self._hip_dot_color = self._hip_dot_color or "f0e982"
	self.hip_dot_color = Color(self._hip_dot_color)
	self.hip_dot_small_w = self.hip_dot_small_w or 3
	self.hip_dot_small_h = self.hip_dot_small_h or 4
	self.hip_dot_small_alpha = self.hip_dot_small_alpha or 0.66
	
	self.hip_dot_medium_w = self.hip_dot_medium_w or 3
	self.hip_dot_medium_h = self.hip_dot_medium_h or 5
	self.hip_dot_medium_alpha = self.hip_dot_medium_alpha or 0.5
	
	local object_name = self.gui_object_name
	if object_name == nil then 
		object_name = "a_o"
	end
--	log("vk78: object_name " .. tostring(object_name or "ERROR bad object name"))
	local object_ids = object_name and Idstring(object_name)
--	log("vk78: object_ids " .. tostring(object_ids or "ERROR bad object ids"))
	local object = object_ids and self._unit:get_object(object_ids)
--	log("vk78: object " .. tostring(object or ("ERROR bad object " .. tostring(object_name))))
	if not object then 
		object = self._unit:orientation_object()
		self.gui_object_name = nil
--		log("vk78: no object found. defaulting to orientation object")
	end
	self.gui_object = object
	
	self:create_panels(object)
end

function VakaraAmmoGui:add_workspace(gui_object,params)
	
	local world_w = params.world_w
	local world_h = params.world_h
	local panel_w = params.panel_w
	local panel_h = params.panel_h
	local offset_x = params.offset_x
	local offset_y = params.offset_y
	local offset_z = params.offset_z
	local yaw = params.yaw
	local pitch = params.pitch
	local roll = params.roll
	
	local ra = gui_object:rotation()
	
	local rb = Rotation(yaw,pitch,roll)
	
	local rot = Rotation(ra:yaw() + rb:yaw(),ra:pitch() + rb:pitch(),ra:roll() + rb:roll())
	
	local x_axis = Vector3(world_w,0,0)
	
	mvector3.rotate_with(x_axis,rot)
	
	local y_axis = Vector3(0,-world_h,0)
	
	mvector3.rotate_with(y_axis,rot)
	
	local center = Vector3(world_w / 2,world_h / 2,0)
	
	mvector3.rotate_with(center,rot)
	
	local offset = Vector3(offset_x,offset_y,offset_z)
	
	mvector3.rotate_with(offset,rot)
	
	local position = gui_object:position()
	
	local new_ws = VakaraAmmoGui.VAKARA_GUI:create_world_workspace(panel_w,panel_h,position,x_axis,y_axis)
	
	new_ws:set_linked(panel_w,panel_h,gui_object,position - center + offset,x_axis,y_axis)
	
	return new_ws
end

function VakaraAmmoGui:create_panels(object)

	if VakaraAmmoGui.VAKARA_GUI and alive(VakaraAmmoGui.VAKARA_GUI) then 
		--nothing
	else
		VakaraAmmoGui.VAKARA_GUI = World:gui()
	end
	
	if object then 
		if self.scope_counter_enabled then 
			local scope_ws = self:add_workspace(object,self._scope_ws_params)
			if scope_ws then 
				self._scope_ws = scope_ws
				self:create_scope_panel(scope_ws)
--				self._scope_ws:hide()
			end
		end
		
		if self.hip_counter_enabled then 
			local hip_ws = self:add_workspace(object,self._hip_ws_params)
			if hip_ws then 
				self._hip_ws = hip_ws
				self:create_hip_panel(hip_ws)
			end
		end
	end
end

function VakaraAmmoGui:create_scope_panel(ws)
	
	local scope_panel = ws:panel():panel({
		name = "scope_panel",
		layer = 1
	})
	self._scope_panel = scope_panel
	
	self._scope_ammo_counter = scope_panel:text({
		name = "scope_ammo_counter",
		font = self.scope_font_name,
		font_size = self.scope_font_size,
		text = "00",
		layer = 3,
		kern = self.scope_kern,
		color = self.scope_font_color,
		x = 76,
		y = 14.75,
		align = "left",
		vertical = "top"
	})
	self._scope_zoom_counter = scope_panel:text({
		name = "scope_zoom_counter",
		font = self.scope_font_name,
		font_size = self.scope_font_size,
		text = "",
		layer = 3,
		kern = self.scope_kern,
		color = self.scope_font_color,
		x = 15,
		y = 14.75,
		align = "left",
		vertical = "top"
	})
	
	local bg_scale = 0.9
--	local bg_ratio = 420/604
	local bg_w = 48 * bg_scale
	local bg_h = 90 * bg_scale
	self._scope_bg_left = scope_panel:bitmap({
		name = "scope_bg_left",
		texture = self.scope_bg_texture,
		texture_rect = {420,0,1024-420,1024},
		rotation = 180,
		layer = 1,
		w = bg_w,
		h = bg_h,
		x = 5,
		y = 2,
		alpha = self.scope_bg_alpha,
		visible = true
	})
	self._scope_bg_right = scope_panel:bitmap({
		name = "scope_bg_right",
		texture = self.scope_bg_texture,
		texture_rect = {420,0,1024-420,1024},
		rotation = 180,
		layer = 1,
		w = -bg_w,
		h = bg_h,
		x = 5 + (2 * bg_w),
		y = 2,
		alpha = self.scope_bg_alpha,
		visible = true
	})
	
	self._scope_panel_debug = scope_panel:rect({
		name = "scope_debug",
		color = Color(1,0.5,0),
		visible = false,
		alpha = 0.1,
		layer = -1
	})
end

function VakaraAmmoGui:create_hip_panel(ws)
	local hip_panel = ws:panel():panel({
		name = "hip_panel",
		layer = 1
	})
	self._hip_panel = hip_panel
	
	self._hip_panel_debug = hip_panel:rect({
		name = "hip_debug",
		color = Color(1,0.5,0),
		visible = false,
		alpha = 0.5,
		layer = -1
	})
	
	self._hip_ammo_counter = hip_panel:text({
		name = "hip_ammo_counter",
		font = self.hip_font_name,
		font_size = self.hip_font_size,
		text = "00",
		monospace = true,
		layer = 3,
		kern = self.hip_kern,
		color = self.hip_font_color,
		x = 2,
		y = 0,
		align = "center",
		vertical = "center"
	})
	
	self._hip_ammo_gradient_top = hip_panel:bitmap({
		name = "hip_ammo_gradient_top",
		texture = self.hip_ammo_gradient_texture,
		w = self.hip_ammo_gradient_w,
		h = -self.hip_ammo_gradient_h,
		x = -1 + (hip_panel:w() - self.hip_ammo_gradient_w) / 2,
		y = 56 - self.hip_ammo_gradient_h,
		color = self.hip_ammo_gradient_color,
		alpha = self.hip_ammo_gradient_alpha,
		layer = 2,
		visible = true
	})
	self._hip_ammo_gradient_bottom = hip_panel:bitmap({
		name = "hip_ammo_gradient_top",
		texture = self.hip_ammo_gradient_texture,
		w = self.hip_ammo_gradient_w,
		h = self.hip_ammo_gradient_h,
		x = -1 + (hip_panel:w() - self.hip_ammo_gradient_w) / 2,
		y = 76,
		color = self.hip_ammo_gradient_color,
		alpha = self.hip_ammo_gradient_alpha,
		layer = 2,
		visible = true
	})
	--[[
	self._hip_text_gradient_top = hip_panel:bitmap({
		name = "hip_text_gradient_top",
		texture = self.hip_text_gradient_texture,
		w = self.hip_text_gradient_w,
		h = -self.hip_text_gradient_h,
		x = (hip_panel:w() - self.hip_text_gradient_w) / 2,
		y = hip_panel:h() / 2,
		color = self.hip_text_gradient_color,
		alpha = self.hip_text_gradient_alpha
	})
	self._hip_text_gradient_bottom = hip_panel:bitmap({
		name = "hip_text_gradient_bottom",
		texture = self.hip_text_gradient_texture,
		w = self.hip_text_gradient_w,
		h = self.hip_text_gradient_h,
		x = (hip_panel:w() - self.hip_text_gradient_w) / 2,
		y = hip_panel:h() / 2,
		color = self.hip_text_gradient_color,
		alpha = self.hip_text_gradient_alpha
	})
	--]]
	
--	local tx,ty,tw,th = self._hip_ammo_counter:text_rect()
		
	self._hip_text_rect_digit_box = hip_panel:rect({
		name = "hip_text_rect_digit_1",
		color = self.hip_text_rect_color,
		w = 42,
		h = 32,
		x = 3,
		y = 40,
		layer = -2,
		alpha = 0.15
	})
	
	self._hip_text_rect_digit_1 = hip_panel:rect({
		name = "hip_text_rect_digit_1",
		color = self.hip_text_rect_color,
		w = self.hip_text_rect_w,
		h = self.hip_text_rect_h,
		x = self._hip_text_rect_digit_box:x() + 2,
		y = self._hip_text_rect_digit_box:y() + 2,
		layer = -1,
		alpha = self.hip_text_rect_alpha
	})
	self._hip_text_rect_digit_2 = hip_panel:rect({
		name = "hip_text_rect_digit_2",
		color = self.hip_text_rect_color,
		w = self.hip_text_rect_w,
		h = self.hip_text_rect_h,
		x = self._hip_text_rect_digit_box:right() - (self.hip_text_rect_w + 2),
		y = self._hip_text_rect_digit_box:y() + 2,
		layer = -1,
		alpha = self.hip_text_rect_alpha
	})
	
	
	
	self._hip_trim_dot_small_1 = hip_panel:rect({
		name = "hip_trim_dot_small_1",
		color = self.hip_dot_color,
		w = self.hip_dot_small_w,
		h = self.hip_dot_small_h,
		x = self._hip_text_rect_digit_box:x() - self.hip_dot_small_w,
		y = self._hip_text_rect_digit_box:y() - self.hip_dot_small_h,
		layer = -2,
		alpha = self.hip_dot_small_alpha
	})
	self._hip_trim_dot_small_2 = hip_panel:rect({
		name = "hip_trim_dot_small_2",
		color = self.hip_dot_color,
		w = self.hip_dot_small_w,
		h = self.hip_dot_small_h,
		x = self._hip_text_rect_digit_box:right(),
		y = self._hip_text_rect_digit_box:y() - self.hip_dot_small_h,
		layer = -2,
		alpha = self.hip_dot_small_alpha
	})
	self._hip_trim_dot_small_3 = hip_panel:rect({
		name = "hip_trim_dot_small_3",
		color = self.hip_dot_color,
		w = self.hip_dot_small_w,
		h = self.hip_dot_small_h,
		x = self._hip_text_rect_digit_box:x() - self.hip_dot_small_w,
		y = self._hip_text_rect_digit_box:bottom(),
		layer = -2,
		alpha = self.hip_dot_small_alpha
	})
	self._hip_trim_dot_small_4 = hip_panel:rect({
		name = "hip_trim_dot_small_4",
		color = self.hip_dot_color,
		w = self.hip_dot_small_w,
		h = self.hip_dot_small_h,
		x = self._hip_text_rect_digit_box:right(),
		y = self._hip_text_rect_digit_box:bottom(),
		layer = -2,
		alpha = self.hip_dot_small_alpha
	})
	
	self._hip_trim_dot_medium_1 = hip_panel:rect({
		name = "hip_trim_dot_medium_1",
		color = self.hip_dot_color,
		w = self.hip_dot_medium_w,
		h = self.hip_dot_medium_h,
		x = self._hip_ammo_gradient_top:x() - self.hip_dot_medium_w,
		y = self._hip_ammo_gradient_top:bottom(),
		layer = -2,
		alpha = self.hip_dot_medium_alpha
	})
	self._hip_trim_dot_medium_2 = hip_panel:rect({
		name = "hip_trim_dot_medium_2",
		color = self.hip_dot_color,
		w = self.hip_dot_medium_w,
		h = self.hip_dot_medium_h,
		x = self._hip_ammo_gradient_top:right(),
		y = self._hip_ammo_gradient_top:bottom(),
		layer = -2,
		alpha = self.hip_dot_medium_alpha
	})
	self._hip_trim_dot_medium_3 = hip_panel:rect({
		name = "hip_trim_dot_medium_3",
		color = self.hip_dot_color,
		w = self.hip_dot_medium_w,
		h = self.hip_dot_medium_h,
		x = self._hip_ammo_gradient_bottom:x() - self.hip_dot_medium_w,
		y = self._hip_ammo_gradient_bottom:bottom(),
		layer = -2,
		alpha = self.hip_dot_medium_alpha
	})
	self._hip_trim_dot_medium_4 = hip_panel:rect({
		name = "hip_trim_dot_medium_4",
		color = self.hip_dot_color,
		w = self.hip_dot_medium_w,
		h = self.hip_dot_medium_h,
		x = self._hip_ammo_gradient_bottom:right(),
		y = self._hip_ammo_gradient_bottom:bottom(),
		layer = -2,
		alpha = self.hip_dot_medium_alpha
	})
	
end

function VakaraAmmoGui:set_scope_visible(visible)
	self.has_scope = visible
	if alive(self._scope_ws) then 
		if visible then 
			self._scope_ws:show()
		else
			self._scope_ws:hide()
		end
	end
end

function VakaraAmmoGui:set_visible(visible)
	if visible then 
		if alive(self._scope_ws) then 
			self._scope_ws:show()
		end
		if alive(self._hip_ws) then 
			self._hip_ws:show()
		end
	else
		if alive(self._scope_ws) then 
			if self.has_scope then 
				self._scope_ws:show()
			else
				self._scope_ws:hide()
			end
		end
		if alive(self._hip_ws) then 
			self._hip_ws:hide()
		end
	end
end

function VakaraAmmoGui:set_ammo_count(current,total)
	local s = string.format("%02i",math.clamp(current,0,self.max_counter_value))
	self:set_scope_ammo_counter(s)
	self:set_hip_ammo_counter(s)
	self:set_hip_ammo_ratio(math.clamp(current/total,0,1))
end

function VakaraAmmoGui:set_zoom(amount)
	if alive(self._scope_zoom_counter) then 
		self._scope_zoom_counter:set_text(string.format("%.01fx",amount))
	end
end

function VakaraAmmoGui:set_scope_ammo_counter(text)
	if alive(self._scope_ammo_counter) then 
		self._scope_ammo_counter:set_text(text)
	end
end

function VakaraAmmoGui:set_hip_ammo_counter(text)
	if alive(self._hip_ammo_counter) then 
		self._hip_ammo_counter:set_text(text)
	end
end

function VakaraAmmoGui:set_hip_ammo_ratio(r)
	local h = self.hip_ammo_gradient_texture_rect[4]
	local texture_rect = {
		self.hip_ammo_gradient_texture_rect[1],
		self.hip_ammo_gradient_texture_rect[2] + (h * (1 - r)),
		self.hip_ammo_gradient_texture_rect[3],
		h * r
	}
	if alive(self._hip_ammo_gradient_bottom) then 
		self._hip_ammo_gradient_bottom:set_texture_rect(unpack(texture_rect))
		self._hip_ammo_gradient_bottom:set_h(self.hip_ammo_gradient_h * r)
	end
	if alive(self._hip_ammo_gradient_top) then 
		self._hip_ammo_gradient_top:set_texture_rect(unpack(texture_rect))
		self._hip_ammo_gradient_top:set_h(-self.hip_ammo_gradient_h * r)
		
	end
end

function VakaraAmmoGui:destroy()
	if alive(VakaraAmmoGui.VAKARA_GUI) then
		if alive(self._scope_ws) then 
			VakaraAmmoGui.VAKARA_GUI:destroy_workspace(self._scope_ws)
		end
		self._scope_ws = nil
		
		if alive(self._hip_ws) then 
			VakaraAmmoGui.VAKARA_GUI:destroy_workspace(self._hip_ws)
		end
		self._hip_ws = nil
	end
end