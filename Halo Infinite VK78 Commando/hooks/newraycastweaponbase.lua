Hooks:PostHook(NewRaycastWeaponBase,"clbk_assembly_complete","vk48_on_assembly_complete",function(self,clbk,parts,blueprint)
	self.registered_vakara_guis = self.registered_vakara_guis or {}
	for part_id,part_data in pairs(parts) do 
		if part_data.unit and alive(part_data.unit) then
			local vakara_gui = part_data.unit:vakara_gui()
			if vakara_gui then 
				self.registered_vakara_guis[part_id] = {
					counter_extension = vakara_gui
				}
				vakara_gui:set_scope_visible(true)
				
				
				--set scope zoom number (WOAH ACTUALLY ACCURATE NOW WOW!!)
				local wtd = tweak_data.weapon
				local zoomstats = wtd.stats.zoom
				local part_tweak_data = wtd.factory.parts[part_id]
				local weapon_td = wtd[self._unit:get_name_id()]
				local weapon_stats = weapon_td and weapon_td.stats
				local zoom_magnification
				if part_tweak_data then
					local part_stats = part_tweak_data.stats
					if part_stats then 
						if part_stats.zoom then 
							zoom_magnification = 1 + part_stats.zoom
						elseif part_stats.gadget_zoom then 
							zoom_magnification = part_stats.gadget_zoom
						elseif part_stats.gadget_zoom_add then
							local current_zoom = weapon_stats and weapon_stats.zoom or 1
							zoom_magnification = current_zoom + part_stats.gadget_zoom_add
						end
					end
					
					if zoom_magnification then 
						zoom_magnification = math.clamp(zoom_magnification, 1, #zoomstats)
						zoom_magnification = zoomstats[1] / zoomstats[zoom_magnification]
						zoom_magnification = math.round(zoom_magnification * zoom_magnification, 0.25)
					end
				end
				if zoom_magnification and zoom_magnification > 1 then 
					vakara_gui:set_zoom(zoom_magnification)
				else
					vakara_gui:set_zoom(1)
				end
			end
		end
	end
end)


Hooks:PostHook(NewRaycastWeaponBase,"_set_parts_enabled","vk48_set_parts_visible",function(self,enabled)
	if self.registered_vakara_guis then 
		for part_id,gui_data in pairs(self.registered_vakara_guis) do 
			if gui_data.counter_extension then
				gui_data.counter_extension:set_visible(enabled)
			end
		end
	end
end)

Hooks:PostHook(NewRaycastWeaponBase,"set_ammo_remaining_in_clip","vk48_set_ammo_counter",function(self,amount)
	if self.registered_vakara_guis then 
		for part_id,gui_data in pairs(self.registered_vakara_guis) do 
			if gui_data.counter_extension then
				gui_data.counter_extension:set_ammo_count(amount,self:get_ammo_max_per_clip())
			end
		end
	end
end)