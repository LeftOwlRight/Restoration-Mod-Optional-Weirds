Hooks:PostHook(WeaponFactoryTweakData, "init", "vk78_commando_init", function(self)
	if self.parts.wpn_fps_ass_vk78_commando_light_am then --wrote this before i coded in the attachment.
		self.parts.wpn_fps_ass_vk78_commando_light_am.has_description = true
		self.parts.wpn_fps_ass_vk78_commando_light_am.custom_stats = {
			rays = 1,
			ammo_pickup_min_mul = 1.20,
			ammo_pickup_max_mul = 1.20,
			armor_piercing_add = 0,
			can_shoot_through_enemy = false,
			can_shoot_through_shield = false,
			can_shoot_through_wall = false,
		}
	end

end)