Hooks:PostHook(WeaponTweakData, "init", "vk78_commandomodInit", function(self)
	--Flashlights
	-- If you're reading this file and want to adjust the positioning of the gadgets
	-- Look at Vector3 (X,Y,Z)
	-- Assuming the weapon is facing you, positive values in X will move the attachment left and negative right, respectively
	-- Positive values in Y will move the attachment forwards and negative backwards, respectively
	-- Positive values in Z will move the attachment up and negative down, respectively
	-- For left/top rails, invert the directions.
	
if self.SetupAttachmentPoint then			
	self:SetupAttachmentPoint( "vk78_commando", {
		name = "a_fl",
		base_a_obj = "a_fl",
		position = Vector3(0.7, 23, 1.6),
		rotation = RotationCAP(0, 0, 0)
	})
	
	self:SetupAttachmentPoint( "vk78_commando", {
        name = "a_left_rail",
        base_a_obj = "a_fl",
        position = Vector3( -5.1, 23, 1.5),
        rotation = RotationCAP( 0, 0, -180 )
    })
	
	self:SetupAttachmentPoint( "vk78_commando", {
        name = "a_top_rail",
        base_a_obj = "a_fl",
        position = Vector3( -2.5, 2, 4.3),
        rotation = RotationCAP( 0, 0, -90 )
    })
	
	self:SetupAttachmentPoint("vk78_commando", {  -- Iron Sights Pack
		name = "a_of",
		base_a_obj = "a_o",
		position = Vector3(0, 20, 0)
	})
	self:SetupAttachmentPoint("vk78_commando", {
		name = "a_of_moe",
		base_a_obj = "a_o",
		position = Vector3(0, 20, 0)
	})
	self:SetupAttachmentPoint("vk78_commando", {
		name = "a_or",
		base_a_obj = "a_o",
		position = Vector3(0, -11, 0)
	})
	self:SetupAttachmentPoint("vk78_commando", {
		name = "a_or_peak",
		base_a_obj = "a_o",
		position = Vector3(0, -11, 0)
	})
	
	self:SetupAttachmentPoint( "vk78_commando", {
        name = "a_lrail",
        base_a_obj = "a_fl",
        position = Vector3( -2.1, 10, 3.0),
        rotation = RotationCAP( 0, 0, 0 )
    })
	
	self:SetupAttachmentPoint( "vk78_commando", {
        name = "a_charm",
        base_a_obj = "a_fl",
        position = Vector3( -6.7, -21, 0.80),
        rotation = RotationCAP( 180, 0, 0 )
    })
	
	
end
end)