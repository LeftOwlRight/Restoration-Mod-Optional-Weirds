{
	"bm_w_m7caseless" : "Misriah Armory M7 SMG (Secondary)",
	"bm_w_m7caseless_desc" : "The UNSC's standard issue submachine gun chambered in 5x23mm Caseless ammunition.",
	"bm_w_m7caseless_prim" : "Misriah Armory M7 SMG (Primary)",
	"bm_w_m7caseless_prim_desc" : "The UNSC's standard issue submachine gun chambered in 5x23mm Caseless ammunition.",
	"bm_w_x_m7caseless" : "Dual Wielded M7 SMGs",
	"bm_w_x_m7caseless_desc" : "Bet'cha can't stick it...",
	"menu_l_global_value_m7caselessmod" : "This is a Misriah Armory Item!",
	
	"bm_menu_internal" : "Internals",
	"bm_menu_internals_plural" : "Internals",
	"bm_menu_counter" : "Counter",
	"bm_menu_counter_plural" : "Counters",
	"bm_menu_gadget_placement" : "Gadget Placement",
	"bm_menu_gadget_placement_plural" : "Gadget Placement",
	"bm_menu_stock_pad" : "Stock Pad",
	"bm_menu_stock_pad_plural" : "Stock Pads",
	"bm_menu_sound" : "Sound Swaps",
	"bm_menu_sound_plural" : "Sound Swaps",
		
	"bm_wp_wpn_fps_smg_m7caseless_h2smg_so" : "Sounds from Halo 2 & Halo 3: ODST",
	"bm_wp_wpn_fps_smg_m7caseless_h2smg_so_desc" : "Swaps the unsuppressed sounds to the Halo 2 original and the suppressed sounds to Halo 3:ODST.",

	"bm_wp_wpn_fps_smg_m7caseless_am_odst" : "M7S Conversion",
	"bm_wp_wpn_fps_smg_m7caseless_am_odst_desc" : "\nReduces ammo pickup rates by 20%.\n\nConversion altering the M7 Caseless to match the performance of the M7S variant in service with the Orbital Drop Shock Troopers.",
	"bm_wp_wpn_fps_smg_m7caseless_scope" : "SLS/V5B Scope (Fixed Reticle)",
	"bm_wp_wpn_fps_smg_m7caseless_scope_desc" : "Vanilla red dot style, albeit with a fixed reticle.",
	"bm_wp_wpn_fps_smg_m7caseless_scope_classic" : "Smart Linked Scope (ACH)",
	"bm_wp_wpn_fps_smg_m7caseless_scope_classic_desc" : "\nREQUIRES WEAPONLIB.\n\nProjects a scope overlay onto the screen, just like in Halo 3 ODST.",
	"bm_wp_wpn_fps_smg_m7caseless_scope_ach" : "Smart Linked Scope (ACH/No Reticle)",
	"bm_wp_wpn_fps_smg_m7caseless_scope_ach_desc" : "\nREQUIRES WEAPONLIB.\n\nProjects a scope overlay onto the screen but with the default reticle removed, for use with your own custom reticle from Advanced Crosshairs, Hitmarkers, and Hitsounds",
	"bm_wp_wpn_fps_smg_m7caseless_suppressor" : "SS/M49 Suppressor w/ Laser Sight",
	"bm_wp_wpn_fps_smg_m7caseless_suppressor_desc" : " ",
	"bm_wp_wpn_fps_smg_m7caseless_stock_collapsed" : "Collapsed Stock",
	"bm_wp_wpn_fps_smg_m7caseless_stock_collapsed_desc" : " ",



}