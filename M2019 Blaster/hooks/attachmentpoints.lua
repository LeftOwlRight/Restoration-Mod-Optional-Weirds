Hooks:PostHook( WeaponTweakData, "init", "lapdHook", function(self)
    if self.lapd then
        self.lapd.attachment_points = {
            {
                name = "a_b", -- Name of Attachment Point ( This can be a preexisting one or a new one. )
                base_a_obj = "a_b", -- Attachment to base the attachment off off. This is so you can attach to the magazine if you want to. ( This defaults to 'a_body' if not included. )
                position = Vector3( -0.2, -3.82, -0.37 ), -- Position to offset from the base_a_obj.
                rotation = Rotation( 0, 0, 0 ) -- Rotation to offset from the base_a_obj.
            },
	{
				name = "a_fl",
				base_a_obj = "a_body",
				position = Vector3(0.6, 6, 0.1),
				rotation = Rotation(0, 0, 0)
	},
	{
				name = "a_lapdrds",
				base_a_obj = "a_body",
				position = Vector3(0.6, 6, 0.1),
				rotation = Rotation(0, 0, 0)
	}


}
    end

    if self.x_lapd then
        self.x_lapd.attachment_points = {
            {
                name = "a_b", -- Name of Attachment Point ( This can be a preexisting one or a new one. )
                base_a_obj = "a_b", -- Attachment to base the attachment off off. This is so you can attach to the magazine if you want to. ( This defaults to 'a_body' if not included. )
                position = Vector3( -0.2, -3.82, -0.37 ), -- Position to offset from the base_a_obj.
                rotation = Rotation( 0, 0, 0 ) -- Rotation to offset from the base_a_obj.
            },
	{
				name = "a_fl",
				base_a_obj = "a_body",
				position = Vector3(0.6, 6, 0.1),
				rotation = Rotation(0, 0, 0)
	},
	{
				name = "a_lapdrds",
				base_a_obj = "a_body",
				position = Vector3(0.6, 6, 0.1),
				rotation = Rotation(0, 0, 0)
	}


}
    end


end )