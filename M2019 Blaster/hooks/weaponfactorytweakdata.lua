Hooks:PostHook( WeaponFactoryTweakData, "init", "lapdModInit", function(self)
self.parts.wpn_fps_pis_lapd_body_standard.stance_mod = {
			wpn_fps_pis_lapd = { 
				translation = Vector3(0, -2, -0.5),
				rotation = Rotation(0.06, -0.1, 1.95413e-011)
			}
		}
self.parts.wpn_fps_pis_lapd_body_black.stance_mod = {
			wpn_fps_pis_lapd = { 
				translation = Vector3(0, -2, 0.7),
				rotation = Rotation(0.06, -0.1, 1.95413e-011)
			}
		}

self.parts.wpn_fps_pis_lapd_body_standard.animations = {
	reload_not_empty = "reload_not_empty",
	reload = "reload"
}
self.parts.wpn_fps_pis_lapd_body_lights1.animations = {
	reload_not_empty = "reload_not_empty",
	reload = "reload"
}
	self.parts.wpn_fps_pis_lapd_a_bronco.custom_stats = {
	ammo_pickup_min_mul = .80,
	ammo_pickup_max_mul = .80
	}

	self.wpn_fps_pis_lapd.adds = {
		wpn_fps_upg_o_specter = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_aimpoint = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_aimpoint_2 = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_docter = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_eotech = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_fl_pis_laser = {
			"wpn_fps_pis_lapd_rail"
		},
		wpn_fps_upg_fl_pis_tlr1 = {
			"wpn_fps_pis_lapd_rail"
		},
		wpn_fps_upg_fl_pis_crimson = {
			"wpn_fps_pis_lapd_rail"
		},
		wpn_fps_upg_fl_pis_x400v = {
			"wpn_fps_pis_lapd_rail"
		},
		wpn_fps_upg_fl_pis_m3x = {
			"wpn_fps_pis_lapd_rail"
		},
		wpn_fps_upg_o_rmr = {
			"wpn_fps_pis_beretta_o_std"
		},
		wpn_fps_upg_o_t1micro = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_cmore = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_acog = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_cs = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_eotech_xps = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_reflex = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_rx01 = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_rx30 = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_spot = {
			"wpn_fps_pis_rage_o_adapter"
		}
	}

self.wpn_fps_pis_x_lapd.adds = {
		wpn_fps_upg_o_specter = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_aimpoint = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_aimpoint_2 = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_docter = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_eotech = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_fl_pis_laser = {
			"wpn_fps_pis_lapd_rail"
		},
		wpn_fps_upg_fl_pis_tlr1 = {
			"wpn_fps_pis_lapd_rail"
		},
		wpn_fps_upg_fl_pis_crimson = {
			"wpn_fps_pis_lapd_rail"
		},
		wpn_fps_upg_fl_pis_x400v = {
			"wpn_fps_pis_lapd_rail"
		},
		wpn_fps_upg_fl_pis_m3x = {
			"wpn_fps_pis_lapd_rail"
		},
		wpn_fps_upg_o_rmr = {
			"wpn_fps_pis_beretta_o_std"
		},
		wpn_fps_upg_o_t1micro = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_cmore = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_acog = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_cs = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_eotech_xps = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_reflex = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_rx01 = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_rx30 = {
			"wpn_fps_pis_rage_o_adapter"
		},
		wpn_fps_upg_o_spot = {
			"wpn_fps_pis_rage_o_adapter"
		}
	}


end )