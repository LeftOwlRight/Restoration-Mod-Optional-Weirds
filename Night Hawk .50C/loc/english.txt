{
    "bm_w_cssdeagle" 		: "Night Hawk .50C",
    "bm_w_cssdeagle_desc" 	: "As expensive as it is powerful, the Night Hawk is an iconic pistol that is difficult to master but surprisingly accurate at long range.",

	"menu_l_global_value_cssdeaglemod" : "This is Counter-Strike: Source item!"
}