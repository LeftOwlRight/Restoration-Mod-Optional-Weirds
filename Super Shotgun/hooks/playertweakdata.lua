Hooks:PostHook(PlayerTweakData, "init", "SSGModInit", function(self)
	if self.stances.super then
		self.stances.super_classic = deep_clone(self.stances.super)
		self.stances.super_classic.standard.shoulders.translation = Vector3(-11.5, -7.299, -1.955)
		self.stances.super_classic.standard.shoulders.rotation = deep_clone(self.stances.super.standard.shoulders.rotation)

		self.stances.super_classic.crouched.shoulders.translation = Vector3(-11.5, -7.299, -1.955)
		self.stances.super_classic.crouched.shoulders.rotation = deep_clone(self.stances.super.standard.shoulders.rotation)

		self.stances.super_classic.steelsight.shoulders.translation = Vector3(-11.5, -7.299, -1.955)
		self.stances.super_classic.steelsight.shoulders.rotation = deep_clone(self.stances.super.standard.shoulders.rotation)
	end
end)