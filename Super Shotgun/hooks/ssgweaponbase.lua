SSGWeaponBase = SSGWeaponBase or class(SaigaShotgun)

function SSGWeaponBase:clbk_assembly_complete(...)
	SSGWeaponBase.super.clbk_assembly_complete(self, ...)

	if table.contains(self._blueprint, "wpn_fps_upg_super_barrel_classic") then
		self:weapon_tweak_data().animations.reload_name_id = "super_classic"
		self:weapon_tweak_data().use_stance = "super_classic"
		self:weapon_tweak_data().weapon_hold = "super_classic"
	end

	if table.contains(self._blueprint, "wpn_fps_upg_super_barrel_doom4") then
		self:weapon_tweak_data().animations.reload_name_id = "super_doom4"
	end
end