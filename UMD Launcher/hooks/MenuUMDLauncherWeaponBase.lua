UMDLauncherWeaponBase = UMDLauncherWeaponBase or class(NewRaycastWeaponBase)

-- More Weapon Stats Support
if Faker then
	ProjectileWeaponBase = ProjectileWeaponBase or class(NewRaycastWeaponBase)

	Faker.classes.UMDLauncherWeaponBase = UMDLauncherWeaponBase
	Faker:redo_class('UMDLauncherWeaponBase', 'ProjectileWeaponBase')
end