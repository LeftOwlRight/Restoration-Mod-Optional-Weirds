Hooks:PostHook(TweakData, "_setup_scene_pose_items", "UMD_Launcher_ScenePoseItems", function(self)
	--This code 'works' but the game doesn't care. Thanks Overkill.
	self.scene_pose_items.husk_umd_launcher = {
		nil,
		"secondary"
	}
end)

Hooks:PostHook(TweakData, "_setup_scene_poses", "UMD_Launcher_ScenePoses", function(self)
	self.scene_poses.weapon.umd_launcher = {
		"husk_umd_launcher",
		required_pose = true
	}
end)